__author__ = 'lennart'
import csv
import numpy as np
from sklearn.svm import LinearSVR


def main():
    # Read training CSV
    X = np.genfromtxt('data/train.csv', delimiter=',', usecols=range(1, 15))
    y = np.genfromtxt('data/train.csv', delimiter=',', usecols=15)

    # Ridge regression
    clf = LinearSVR()
    clf.fit(X, y)

    # Read test CSV
    T_ids = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=0).astype('int')
    T = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=range(1, 15))
    pred = clf.predict(T)

    # Write test prediction
    with open('prediction.csv', 'w') as fp:
        a = csv.writer(fp, delimiter=',')
        rows = zip(T_ids, pred)
        a.writerows([['Id', 'Delay']])
        a.writerows(rows)


if __name__ == "__main__":
    main()