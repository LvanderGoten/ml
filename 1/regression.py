__author__ = 'lennart'
import csv
import numpy as np
from sklearn.linear_model import Ridge
from sklearn.lda import LDA


def main():
    # Read training CSV
    X = np.genfromtxt('data/train.csv', delimiter=',', usecols=range(1, 15))
    y = np.genfromtxt('data/train.csv', delimiter=',', usecols=15)

    # LDA
    clf = LDA(n_components=5)
    X_new = clf.fit_transform(X, y.astype(int))

    # Ridge regression
    clf_ridge = Ridge()
    clf_ridge.fit(X_new, y)

    # Read test CSV
    T_ids = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=0).astype('int')
    T = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=range(1, 15))

    T_proj = clf.transform(T)
    pred = clf_ridge.predict(clf.transform(T))

    # Write test prediction
    with open('prediction.csv', 'w') as fp:
        a = csv.writer(fp, delimiter=',')
        rows = zip(T_ids, pred)
        a.writerows([['Id', 'Delay']])
        a.writerows(rows)


if __name__ == "__main__":
    main()