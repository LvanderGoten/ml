#__author__ = 'lennart'
from __future__ import division
import csv
import numpy as np
from sklearn.svm import LinearSVR
from sklearn import linear_model
from sklearn.linear_model import Ridge
from sklearn.linear_model import Lasso
from sklearn.linear_model import LassoCV
from sklearn.linear_model import LassoLarsIC
import math
import random





def CV10(alpha, X,y):
    '''
    Splits the data into 10 buckets of equal size (except for the last bucket which might be longer if sample_size % 10 != 0)
    '''

    sample_size = int(X.shape[0])
    # define train and test samples sizes
    train_block_size = int(sample_size/10)
    test_block_size = sample_size - train_block_size*9

    # Performing the CV 10 times

    for i in range (0,10):
        a = i * train_block_size
        if i==9:
            b = a + test_block_size
        else:
            b = a + train_block_size

        # Split data

        X_test = X[a:b,:]
        y_test = y[a:b]

        X_train = np.concatenate((X[0:a,:], X[b:,:]),0)
        y_train = np.concatenate((y[0:a], y[b:]),1)


        # Test different models here

        # LASSO
        # clf = Lasso(alpha=1)

        # LASSO Lars with BIC
        # clf = LassoLarsIC(criterion='bic')

        # LASSO regression
        # clf = LassoCV()

        # Ridge regression
        clf = Ridge(alpha)

        clf.fit(X_train,y_train)
        y_pred = clf.predict(X_test)

        computeRMSE(i, y_test, y_pred)
    #print rmse_CV
    [mean, variance] = displayVarRMSE()
    return (mean, variance)


rmse_CV = np.zeros(shape=(10,1))


def computeRMSE(i,y_test, y_pred):
    '''
    Compute the RMSE for the current cross-validation iteration
    '''

    global rmse_CV
    sum_error = 0.0
    rmse = 0.0
    nSamples = y_test.shape[0]

    for j in range(0, nSamples):
        sum_error += (y_test[j]-y_pred[j])**2

    rmse = math.sqrt((1/nSamples)*sum_error)
    # Add the RMSE for the ith CV iteration
    rmse_CV[i] = rmse

def displayVarRMSE():
    '''
    Displays The mean/std/var RMSE of the 10 cross-validation iteration
    '''

    mean = np.mean(rmse_CV)
    stdDev = np.std(rmse_CV)
    variance = np.var(rmse_CV)

    print 'Mean of RMSE : %.5f' % mean
    print 'Standard Deviation of RMSE : %.5f' % stdDev
    print 'Variance of RMSE : %.5f' % variance
    print ' ---------------------- '
    return [mean, variance]


###############################
# Random Transformations
###############################

# Log on first featurem sqrt on all the others
def bestModel5(X,y, f_list, train, gen_indices):
    '''
    experimental. Seems to give the lowest RMSE (~620). To try on Kaggle.
    '''

    # Init rmse_CV
    global rmse_CV
    rmse_CV = np.zeros(shape=(10,1))


    X = np.delete(X, f_list, 1)

    for i in range(1,6):
        #inv = (1.0/X[:,i]).reshape(X.shape[0],1)
        # invLog = (1.0/np.log10(1+X[:,i])).reshape(X.shape[0],1)
        log = np.log10(1+X[:,i]).reshape(X.shape[0],1)
        root = np.sqrt(X[:,i]).reshape(X.shape[0],1)
        square = np.multiply(X[:,i], X[:,i]).reshape(X.shape[0],1)
        cube = (X[:,i]*X[:,i]*X[:,i]).reshape(X.shape[0],1)
        

        #X = np.append(X,inv, axis=1)
        X = np.append(X,log, axis=1)
        # X = np.append(X,invLog, axis=1)
        X = np.append(X,root, axis=1)
        X = np.append(X,square, axis=1)
        X = np.append(X,cube, axis=1)
        print X.shape


    print 'mofo'
    X_temp = X
    for i in range(0, X.shape[1]):
        for j in range (i+1, X.shape[1]):
            X_temp = np.append(X_temp,(X[:,i]*X[:,j]).reshape(X.shape[0],1), axis=1)
        print 'i=%d' % i

    print 'mofo2'

    for i in range(0, X.shape[1]):
        for j in range (0, X.shape[1]):
            if(i != j):
                X_temp = np.append(X_temp,np.divide(X[:,i],X[:,j]).reshape(X.shape[0],1), axis=1)
        print 'i=%d' % i

    X = X_temp
    print 'step 1'
    print 'size: %d' % X.shape[1]

    # array of feature means
    mu = np.mean(X,axis=0)
    # array of feature std deviations
    sigma = np.std(X,axis=0)
    
    print 'step 2'
    print mu.shape    
    print sigma.shape
    # Normalize
    for col in range (0,X.shape[1]):
        # print 'col=%d' % col
        feat_normed = (X[:,col] - mu[col]) / sigma[col]
        X[:,col] = feat_normed

    print 'nan'
    print X[np.isnan(X) == True].shape
    X[np.isnan(X) == True] = 0
    #input("Press Enter to continue...")
    ###############
    #  Uncomment the next line to produce the .csv file, see note in main()
    #  and comment the line CV10(X_tild,y)
    ###############

    #return X_tild
    # ---------> added minVar here (flo)
    minMean = 999999999.0
    minVar = 99999999.0
    bestAlpha = -1
    if train:
        star_indices = [0]
        bestIndex = 0
        # ---------> added bias here (flo)
        X_star = np.empty(shape=(667,0))
        X_star = np.insert(X_star,0,1, axis=1)
        #X_star = X[:,0]
    
        print 'step 3'
        print X.shape # ---------> changed to 23 here  (flo)
        for j in range(1, 21):
            print 'j=%d' % j
            for i in range(1, X.shape[1]):
                if i not in star_indices:
                    for alpha in np.arange(0.1, 2.1, 0.1):
                        print 'alpha=%.1f' % alpha
                        print 'j=%d' % j
                        print 'i=%d' % i
                        print X_star.shape
                        X_test = np.c_[X_star, X[:,i]]
                        #X_test = np.append(X_star, X[:,i], axis=1)
                        print 'appended'
                        print X_test.shape
                        [mean, variance] = CV10(alpha, X_test,y)
                        # ---------> changed condition here
                        #if minMean > mean and minVar > variance:
                        if minMean > mean and minVar > variance:
                            bestIndex = i
                            minMean = mean
                            minVar = variance
                            bestAlpha = alpha
                            print 'new best mean : %.5f' % minMean
                    #X_test = np.delete(X_test, i, 1)
            # X_star[:,j] = np.append(X_star, X[:,bestIndex], axis=1)
            X_star = np.c_[X_star, X[:,bestIndex]]
            star_indices = np.r_[star_indices, bestIndex]
            
        #return X_star
            print X_star.shape
            print star_indices.shape
        print 'minMean is %.5f' % minMean
        print 'Variance is %.5f' % minVar
        print 'alpha is %.1f' % bestAlpha

        print star_indices.shape
        print X_star.shape
        # print star_indices.reshape(0,21)
        # ---------> This doesnt work :
        # return np.r_[X_star, star_indices.reshape(0,23)]
        return (X_star, star_indices, bestAlpha)
    else:
        return X[:,gen_indices.astype(int)]




# Log on first featurem sqrt on all the others
def bestModel4(X,y, f_list):
    '''
    experimental. Seems to give the lowest RMSE (~620). To try on Kaggle.
    '''

    # Init rmse_CV
    global rmse_CV
    rmse_CV = np.zeros(shape=(10,1))

    #feature_log = X[:,0]*X[:,0]*X[:,0]
    feature_log = 1.0/np.log10(1+X[:,0])
    X[:,0] = feature_log

    feature_inv1 = 1.0/(X[:,1])
    X[:,1] = feature_inv1

    feature_inv3 = 1.0/(X[:,3])
    X[:,3] = feature_inv3

    feature_inv5 = 1.0/(X[:,5])
    X[:,5] = feature_inv5

    # feature_inv1 = X[:,1]*X[:,1]*X[:,1]
    # X[:,1] = feature_inv1

    # feature_inv3 = X[:,3]*X[:,3]*X[:,3]
    # X[:,3] = feature_inv3

    # feature_inv5 = X[:,5]*X[:,5]*X[:,5]
    # X[:,5] = feature_inv5

    #feature_sqrt13 = X[:,13]*X[:,13]*X[:,13]
    feature_sqrt13 = np.sqrt(X[:,13])
    X[:,13] = feature_sqrt13

    # make new feature with feat 1 and 14

    # make new feature with feat 1 and 14
    comb1 = np.multiply(X[:,0],X[:,0]).reshape(X.shape[0],1) # keep
    # comb2 = np.multiply(X[:,0],X[:,1]).reshape(X.shape[0],1) 
    comb3 = np.multiply(X[:,0],X[:,3]).reshape(X.shape[0],1) # keep
    comb4 = np.multiply(X[:,0],X[:,5]).reshape(X.shape[0],1) # keep
    comb5 = np.multiply(X[:,0],X[:,13]).reshape(X.shape[0],1) # keep
    comb6 = np.multiply(X[:,1],X[:,1]).reshape(X.shape[0],1)
    # comb7 = np.multiply(X[:,1],X[:,3]).reshape(X.shape[0],1)
    comb8 = np.multiply(X[:,1],X[:,5]).reshape(X.shape[0],1)
    comb9 = np.multiply(X[:,1],X[:,13]).reshape(X.shape[0],1) # keep
    comb10 = np.multiply(X[:,3],X[:,3]).reshape(X.shape[0],1)
    # comb11 = np.multiply(X[:,3],X[:,5]).reshape(X.shape[0],1)
    comb12 = np.multiply(X[:,3],X[:,13]).reshape(X.shape[0],1)

    # make new feature with feat 1 and 14
    comb13 = np.multiply(X[:,5],X[:,5]).reshape(X.shape[0],1) # keep
    comb14 = np.multiply(X[:,5],X[:,13]).reshape(X.shape[0],1) # keep
    # comb15 = np.multiply(X[:,13],X[:,13]).reshape(X.shape[0],1)


    # comb16 = (X[:,0]*X[:,5]*X[:,13]).reshape(X.shape[0],1)

    X = np.append(X,comb1, axis=1)
    # X = np.append(X,comb2, axis=1)
    X = np.append(X,comb3, axis=1)
    X = np.append(X,comb4, axis=1)
    X = np.append(X,comb5, axis=1)
    X = np.append(X,comb6, axis=1)
    # X = np.append(X,comb7, axis=1)
    X = np.append(X,comb8, axis=1)
    X = np.append(X,comb9, axis=1)
    X = np.append(X,comb10, axis=1)
    # X = np.append(X,comb11, axis=1)
    X = np.append(X,comb12, axis=1)
    X = np.append(X,comb13, axis=1)
    X = np.append(X,comb14, axis=1)
    # X = np.append(X,comb15, axis=1)
    # X = np.append(X,comb16, axis=1)



  

    # array of feature means
    mu = np.mean(X,axis=0)
    # array of feature std deviations
    sigma = np.std(X,axis=0)

    # Normalize
    for col in range (0,25):
        feat_normed = (X[:,col] - mu[col]) / sigma[col]
        X[:,col] = feat_normed

    X_tild = np.delete(X, f_list, 1)

    ###############
    #  Uncomment the next line to produce the .csv file, see note in main()
    #  and comment the line CV10(X_tild,y)
    ###############

    return X_tild

    #CV10(X_tild,y)


# Log on first featurem sqrt on all the others
def bestModel3(X,y, f_list):
    '''
    --
    '''

    # Init rmse_CV
    global rmse_CV
    rmse_CV = np.zeros(shape=(10,1))

    feature_log = 1.0/np.log10(1+X[:,0])
    X[:,0] = feature_log

    feature_inv1 = 1.0/(X[:,1])
    X[:,1] = feature_inv1

    feature_inv3 = 1.0/(X[:,3])
    X[:,3] = feature_inv3

    feature_inv5 = 1.0/(X[:,5])
    X[:,5] = feature_inv5

    feature_sqrt13 = np.sqrt(X[:,13])
    X[:,13] = feature_sqrt13

    # make new feature with feat 1 and 14
    comb = np.multiply(X[:,0],X[:,13]).reshape(X.shape[0],1)

    # make new feature with feat 1 and 14
    #comb2 = np.multiply(X[:,0],X[:,3]).reshape(X.shape[0],1)

    # make new feature with feat 1 and 14
    #comb3 = np.multiply(X[:,5],X[:,13]).reshape(X.shape[0],1)

    X = np.append(X,comb, axis=1)
    #X = np.append(X,comb2, axis=1)
   # X = np.append(X,comb3, axis=1)

  

    # array of feature means
    mu = np.mean(X,axis=0)
    # array of feature std deviations
    sigma = np.std(X,axis=0)

    # Normalize
    for col in range (0,15):
        feat_normed = (X[:,col] - mu[col]) / sigma[col]
        X[:,col] = feat_normed

    X_tild = np.delete(X, f_list, 1)

    #return X_tild

    CV10(X_tild,y)


# Log on first featurem sqrt on all the others
def bestModel2(X,y, f_list):
    '''
    --
    '''

    # Init rmse_CV
    global rmse_CV
    rmse_CV = np.zeros(shape=(10,1))

    feature_log = 1.0/np.log10(1+X[:,0])
    X[:,0] = feature_log

    feature_inv = 1.0/X[:,1]
    X[:,1] = feature_inv

    feature_inv = 1.0/X[:,3]
    X[:,3] = feature_inv

    feature_inv = 1.0/X[:,5]
    X[:,5] = feature_inv

    #feature_log = np.log10(X[:,13])
    feature_log = np.sqrt(X[:,13])
    X[:,13] = feature_log

    #comb = X[:,0]*X[:,13].reshape(667,1)

    for col in range (1, 13):
        if col != 1 and col != 3 and col != 5:
            feature_sqrt = 1/np.sqrt(X[:,col])
            X[:,col] = feature_sqrt

    # make new feature with feat 1 and 14
    comb = np.multiply(X[:,0],X[:,13]).reshape(X.shape[0],1)

    comb2 = np.multiply(X[:,1],X[:,5]).reshape(X.shape[0],1)

    X = np.append(X,comb, axis=1)
    X = np.append(X,comb2, axis=1)

  

    # array of feature means
    mu = np.mean(X,axis=0)
    # array of feature std deviations
    sigma = np.std(X,axis=0)

    # Normalize
    for col in range (0,16):
        feat_normed = (X[:,col] - mu[col]) / sigma[col]
        X[:,col] = feat_normed

    X_tild = np.delete(X, f_list, 1)

    #return X_tild

    CV10(X_tild,y)


# Log on first featurem sqrt on all the others
def bestModel(X,y, f1, f2, f3):
    '''
    --
    '''

    # Init rmse_CV
    global rmse_CV
    rmse_CV = np.zeros(shape=(10,1))

    feature_log = 1.0/np.log10(1+X[:,0])
    X[:,0] = feature_log

    feature_inv = 1.0/X[:,1]
    X[:,1] = feature_inv

    feature_inv = 1.0/X[:,3]
    X[:,3] = feature_inv

    feature_inv = 1.0/X[:,5]
    X[:,5] = feature_inv

    #feature_log = np.log10(X[:,13])
    feature_log = np.sqrt(X[:,13])
    X[:,13] = feature_log

    #comb = X[:,0]*X[:,13].reshape(667,1)

    for col in range (1, 13):
        if col != 1 and col != 3 and col != 5:
            feature_sqrt = 1/np.sqrt(X[:,col])
            X[:,col] = feature_sqrt

    comb = np.multiply(X[:,0],X[:,13]).reshape(667,1)

    # print X[0:10,0]
    # print X[0:10,1]
    # print X[0:10,3]
    # print X[0:10,5]
    # print X[0:10,13]
    #print X[0:10,13]
    print comb[0:10]

    # print comb.shape
    # print X.shape

    X_tild = np.append(X,comb, axis=1)

    print X_tild.shape

    

    # array of feature means
    mu = np.mean(X_tild,axis=0)
    # array of feature std deviations
    sigma = np.std(X_tild,axis=0)

    # Normalize
    for col in range (0,15):
        feat_normed = (X_tild[:,col] - mu[col]) / sigma[col]
        X_tild[:,col] = feat_normed

    X_tild2 = np.delete(X_tild, [f1-1, f2-1, f3-1], 1)

    #return X_tild

    CV10(X_tild2,y)

def removeOneFeature(X,y):
    '''
    Tests the regression by removing 1 feature at a time
    Note : Same as below
    '''

    for f in range (0,X.shape[1]):
        print 'Removing feature %s' % `f+1`
        X_tild = np.delete(X, f, 1)
        sqrtFeaturesRegression(X_tild,y, 13)


def removeTwoFeatures(X,y):
    '''
    Tests the regression by removing 2 features at a time
    Note : Results show that features 1 and 14 are important
    '''

    global currentFeaturesRemoved

    for f1 in range (0,X.shape[1]):
        for f2 in range(f1+1, X.shape[1]):
            print 'Removing feature %s and %s' % (`f1+1`, `f2+1`)
            currentFeaturesRemoved = (f1,f2)
            X_tild = np.delete(X, [f1,f2], 1)
            sqrtFeaturesRegression(X_tild, y, 12)

################################

def main():
    # Read training CSV
    X = np.genfromtxt('data/train.csv', delimiter=',', usecols=range(1, 15))
    y = np.genfromtxt('data/train.csv', delimiter=',', usecols=15)

    ##############

    #sqrtFeaturesRegression(X, y, 14)
    #logFirstSqrtOthersRegression(X, y, 14)
    #logFirstSqrtOthersRemove2FeatRegression(X, y, 7, 10)
    #X_tild = log1Inv4SqrtOthersRemove2FeatRegression(X,y, 9, 10)

    #log1Inv4SqrtOthersRemove3FeatRegression(X,y, 7, 9, 10)
    #X_tild = bestModel5(X,y,[2,4,6,7,8,9,10,11,12])
    [X_tild, star_indices, alpha] = bestModel5(X, y, [2,4,6,7,8,9,10,11,12], 1, [])
    # star_indices = X_tild[X_tild.shape[0]-1,:]
    # print 'star_indices'
    # print star_indices
    # X_tild = np.delete(X_tild, X_tild.shape[0]-1, 0)
    # #log1Inv4SqrtOthersRegression(X,y)
    # #removeOneFeature(X,y)
    # #removeTwoFeatures(X,y)

    # # ############
    clf = Ridge(alpha)
     #clf = LassoLarsIC(criterion='bic')
    clf.fit(X_tild,y)


    # # # Read test CSV
    T_ids = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=0).astype('int')
    T = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=range(1, 15))

    T_tild = bestModel5(T,y,[2,4,6,7,8,9,10,11,12], 0, star_indices)
    pred = clf.predict(T_tild)

     # # Write test prediction
    with open('prediction_flo5.csv', 'w') as fp:
        a = csv.writer(fp, delimiter=',')
        rows = zip(T_ids, pred)
        a.writerows([['Id', 'Delay']])
        a.writerows(rows)


if __name__ == "__main__":
    main()