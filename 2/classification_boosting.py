#__author__ = 'lennart'
from __future__ import division
import csv
import numpy as np
import math
import random
import time
import datetime
from sknn.mlp import Classifier, Layer
from sklearn import svm, grid_search
from sklearn.cross_validation import cross_val_score
from sklearn.ensemble import AdaBoostClassifier, GradientBoostingClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn import cross_validation




def CV10(X,y):
    '''
    Splits the data into 10 buckets of equal size (except for the last bucket which might be longer if sample_size % 10 != 0)
    '''

    results = []

    sample_size = int(X.shape[0])
    # define train and test samples sizes
    train_block_size = int(sample_size/10)
    test_block_size = sample_size - train_block_size*9

    # Performing the CV 10 times

    for i in range (0,10):
        a = i * train_block_size
        if i==9:
            b = a + test_block_size
        else:
            b = a + train_block_size

        # Split data

        X_test = X[a:b,:]
        y_test = y[a:b]

        X_train = np.concatenate((X[0:a,:], X[b:,:]),0)
        y_train = np.concatenate((y[0:a], y[b:]),0)


        C = np.arange(0.1, 5.1, 0.5)
        gamma = np.arange(0.00001, 0.4, 0.1)

        # Test different models here

        parameters_bc = {'max_samples':[0.7, 0.8, 0.9, 1.0], 'max_features':[0.5, 0.6, 0.7, 0.8, 0.9, 1.0]}
        parameters_svc = {'C':C, 'gamma':gamma}

        svc_c = svm.SVC()
        svc_clf = grid_search.GridSearchCV(svc_c, parameters_svc)
        svc_clf.fit(X_train, y_train)

        print 'Best params for SVC :'
        print svc_clf.best_params_
        
        adaboost = AdaBoostClassifier(
            base_estimator=svm.SVC(C=svc_clf.best_params_['C'], gamma=svc_clf.best_params_['gamma']),
            n_estimators=100,
            algorithm='SAMME.R')



        # This function does crossvalidation, but here it does cross-validation over 'our' cross validation.
        # Can be useful when we don't use our CV.
        # --->
        # scores = cross_val_score(clf, X_train, y_train)
        #print 'Score mean : %.5f' % scores.mean() 

        adaboost.fit(X_train,y_train)

        print 'Boosting score :'
        print adaboost.score(X_train, y_train)

        y_pred = adaboost.predict(X_test)

        # End model test
        return y_pred



###############################
# Prediction
###############################

def prediction(X,y,T):

    # C = np.arange(0.1, 5.1, 0.5)
    # gamma = np.arange(0.00001, 0.4, 0.1)
    C = np.linspace(0.1, 5.0, 50)
    gamma = np.linspace(0.00001, 0.4, 100)
    # C = 4.0999999999999996
    # gamma = 0.064654848484848482

    # Test different models here

    parameters_bc = {'max_samples':[0.7, 0.8, 0.9, 1.0], 'max_features':[0.5, 0.6, 0.7, 0.8, 0.9, 1.0]}
    parameters_svc = {'C':C, 'gamma':gamma}


    # clf = grid_search.GridSearchCV(estimator=LogisticRegression(penalty='l2', solver='lbfgs', multi_class='multinomial'), param_grid=dict(C=''),
    #                    cv=cross_validation.ShuffleSplit(2025, n_iter=100, test_size=0.05),
    #                    n_jobs=-1)

    # clf.fit(X, y)

    # print 'Best params for LR :'
    # print clf.best_params_

    # svc_c = svm.SVC()
    # svc_clf = grid_search.GridSearchCV(svc_c, parameters_svc)
    # svc_clf.fit(X, y)

    # print 'Best params for SVC :'
    # print svc_clf.best_params_
    
    # adaboost = AdaBoostClassifier(
    #     base_estimator=svm.SVC(C=C, gamma=gamma,),
    #     n_estimators=100,
    #     algorithm='SAMME')
    # adaboost = AdaBoostClassifier(
    #     base_estimator=LogisticRegression(
    #         penalty='l2',
    #         C=4.2,
    #         solver='lbfgs',
    #         multi_class='multinomial'
    #         ),
    #     n_estimators=100,
    #     algorithm='SAMME.R')

    weights = {0: 1, 1: 1, 2: 3}

    # # parameters_dtc = {'min_samples_leaf':[1,2,3,4,5,6,7,8,9,10], 'max_features':np.linspace(0.1, 1.0, 20), 'min_samples_split':[2,3,4,5]}
    # parameters_gbc = {
    # 'learning_rate':np.linspace(0.01, 0.3, 10), 
    # 'min_samples_split':[2,3,4,5], 
    # 'min_samples_leaf':[1,2,3,4,5,6,7,8,9,10],
    # 'subsample':np.linspace(0.1, 1, 5)
    # }

    # # clf = grid_search.GridSearchCV(DecisionTreeClassifier(max_features=0.72, min_samples_split=4, min_samples_leaf=7, class_weight=weights), parameters_dtc)
    # clf = grid_search.GridSearchCV(
    #     GradientBoostingClassifier(), parameters_gbc)

    # clf.fit(X,y)

    # print 'Best params for DTC :'
    # print clf.best_params_

    # adaboost = AdaBoostClassifier(
    #     base_estimator=DecisionTreeClassifier(
    #         min_samples_leaf=clf.best_params_['min_samples_leaf'], 
    #         max_features=clf.best_params_['max_features'], 
    #         min_samples_split=clf.best_params_['min_samples_split'],
    #         class_weight=weights),
    #     n_estimators=200,
    #     algorithm='SAMME.R')
    # adaboost = AdaBoostClassifier(
    #     base_estimator=DecisionTreeClassifier(
    #         criterion='gini',
    #         splitter='best',
    #         min_samples_leaf=7, 
    #         max_features=0.7, 
    #         min_samples_split=4,
    #         class_weight=weights),
    #     n_estimators=500,
    #     algorithm='SAMME.R')


    # print 'Best params for GBC :'
    # print clf.best_params_

    # gradBoost = GradientBoostingClassifier(
    #     loss='deviance',
    #     learning_rate=clf.best_params_['learning_rate'],
    #     min_samples_split=clf.best_params_['min_samples_split'],
    #     min_samples_leaf=clf.best_params_['min_samples_leaf'],
    #     subsample=clf.best_params_['subsample']
    #     )


    svc_c = LogisticRegression(C=4.3838383838383841, fit_intercept=True, class_weight=weights)
    svc_c.fit(X,y)




    gradBoost = GradientBoostingClassifier(
        init=svc_c,
        loss='deviance',
        learning_rate=0.074444,
        min_samples_split=3,
        min_samples_leaf=7,
        subsample=0.1,
        n_estimators=200,
        )



    gradBoost.fit(X, y)


    scores = cross_val_score(gradBoost, X, y, cv=3)
    print 'Score mean : %.5f' % scores.mean() 



    print 'Feature importance :'
    print gradBoost.feature_importances_

    y_pred = gradBoost.predict(T)

    # End model test
    return y_pred


class init:
    def __init__(self, est):
        self.est = est
    def predict(self, X):
        return self.est.predict_proba(X)[:,1][:,numpy.newaxis]
    def fit(self, X, y, sample_weight=None):
        self.est.fit(X, y)

###############################
# Feature Transformations
###############################

def attempt1(X):

    feature1= 1.0/np.log10(1+X[:,0])
    X[:,0] = feature1

    # feature2 = np.log10(1+X[:,1])
    # X[:,1] = feature2

    feature2 = np.sqrt(X[:,1])
    X[:,1] = feature2

    feature3 = np.sqrt(X[:,2])
    X[:,2] = feature3


    feature5 = 1.0/X[:,4]
    X[:,4] = feature5

    # feature1 = np.sqrt(X[:,0])
    # X[:,0] = feature1

    feature7 = np.sqrt(X[:,6])
    X[:,6] = feature7

    # combi1 = np.multiply(feature1,feature5).reshape(X.shape[0],1)
    combi2 = np.multiply(feature1,feature2).reshape(X.shape[0],1)

    # X = np.append(X, combi1, axis=1)
    X = np.append(X, combi2, axis=1)

    mu_feat = np.mean(X, axis=0)
    sigma_feat = np.std(X, axis=0)

    for i in range (1, X.shape[1]):
        X[:,i] = (X[:,i]-mu_feat[i])/sigma_feat[i]

    return X

def remove1Feature(X,y):
    ## Normalization of the data

    for f in range (0,X.shape[1]):
        print 'Removing feature %s' % `f+1`
        X_tild = np.delete(X, f, 1)

        mu_feat = np.mean(X_tild, axis=0)
        sigma_feat = np.std(X_tild, axis=0)

        for i in range (0, X_tild.shape[1]):
            X_tild[:,i] = (X_tild[:,i]-mu_feat[i])/sigma_feat[i]

        CV10(X_tild,y)


################################

def main():

    train = True

    # Read training CSV
    X = np.genfromtxt('data/train.csv', delimiter=',', usecols=range(1, 8))
    y = np.genfromtxt('data/train.csv', delimiter=',', usecols=8)

    # # # Read test CSV
    T_ids = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=0).astype('int')
    T = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=range(1, 8))

    X_transf = attempt1(X)

    T_transf = attempt1(T)

    y_pred = prediction(X_transf, y, T_transf)

   
     # # Write test prediction
    with open('prediction_adaboost.csv', 'w') as fp:
        a = csv.writer(fp, delimiter=',')
        rows = zip(T_ids, y_pred.astype('int'))
        a.writerows([['Id', 'Label']])
        a.writerows(rows)


if __name__ == "__main__":
    main()