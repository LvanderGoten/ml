__author__ = 'lennart'
import csv
import numpy as np
from sklearn import preprocessing
from sklearn.svm import SVC
from sklearn.grid_search import GridSearchCV
from sklearn import cross_validation
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn import linear_model

from sklearn.cross_validation import cross_val_score


def attempt2(X):

    feature1= 1.0/np.log10(1+X[:,0])
    X[:,0] = feature1

    feature2 = np.sqrt(X[:,1])
    X[:,1] = feature2

    feature3 = np.sqrt(X[:,2])
    X[:,2] = feature3

    feature4 = 1.0/np.log10(1+X[:,3])
    X[:,3] = feature4

    feature5 = 1.0/X[:,4]
    X[:,4] = feature5

    feature6 = X[:,5]

    feature7 = np.sqrt(X[:,6])
    X[:,6] = feature7

    combi1 = np.multiply(feature1,feature5).reshape(X.shape[0],1)
    combi2 = np.multiply(feature1,feature2).reshape(X.shape[0],1)
    combi3 = np.multiply(feature1,feature3).reshape(X.shape[0],1)
    combi4 = np.multiply(feature1,feature4).reshape(X.shape[0],1)
    combi5 = np.multiply(feature1,feature7).reshape(X.shape[0],1)
    combi6 = np.multiply(feature2,feature3).reshape(X.shape[0],1)
    combi7 = np.divide(feature1,feature5).reshape(X.shape[0],1)
    # combi8 = np.divide(feature1,feature2).reshape(X.shape[0],1)
    combi9 = np.divide(feature2,feature3).reshape(X.shape[0],1)
    combi10 = np.divide(feature4,feature2).reshape(X.shape[0],1)
    # combi11 = np.divide(feature5,feature6).reshape(X.shape[0],1)
    # combi12 = np.divide(feature1,feature5).reshape(X.shape[0],1)

    X = np.append(X, combi1, axis=1)
    X = np.append(X, combi2, axis=1)
    X = np.append(X, combi3, axis=1)
    X = np.append(X, combi4, axis=1)
    X = np.append(X, combi5, axis=1)
    X = np.append(X, combi6, axis=1)
    X = np.append(X, combi7, axis=1)
    # X = np.append(X, combi8, axis=1)
    X = np.append(X, combi9, axis=1)
    X = np.append(X, combi10, axis=1)
    # X = np.append(X, combi11, axis=1)
    # X = np.append(X, combi12, axis=1)

    mu_feat = np.mean(X, axis=0)
    sigma_feat = np.std(X, axis=0)

    for i in range (1, X.shape[1]):
        X[:,i] = (X[:,i]-mu_feat[i])/sigma_feat[i]

    return X

def attempt1(X):
    feature1 = 1.0 / np.log10(1 + X[:, 0])
    X[:, 0] = feature1

    feature2 = np.log10(1 + X[:, 1])
    X[:, 1] = feature2

    feature3 = np.sqrt(X[:, 2])
    X[:, 2] = feature3

    feature5 = 1.0 / X[:, 4]
    X[:, 4] = feature5

    feature7 = np.sqrt(X[:, 6])
    X[:, 6] = feature7

    combi1 = np.multiply(feature1, feature5).reshape(X.shape[0], 1)

    X = np.append(X, combi1, axis=1)

    mu_feat = np.mean(X, axis=0)
    sigma_feat = np.std(X, axis=0)

    for i in range(0, X.shape[1]):
        X[:, i] = (X[:, i] - mu_feat[i]) / sigma_feat[i]

    return X


def main():


    class_weight = {0:1, 1:1, 2:3}
    # Read training CSV
    X = np.genfromtxt('data/train.csv', delimiter=',', usecols=range(1, 8))
    y = np.genfromtxt('data/train.csv', delimiter=',', usecols=8)

    # Standardize data
    X = attempt2(X)

    # Grid search using SVM & CV
    # svc = SVC(kernel='rbf', C=4.3838383838383841, gamma=0.063333333333333339, random_state=0)
    # svc.fit(X,y)
    svc = linear_model.SGDClassifier(loss='hinge',alpha=0.00304, learning_rate='optimal', power_t=0.85306122449, average=21 )
    svc.fit(X, y)

    scores = cross_val_score(svc, X, y)
    print 'Score mean : %.5f' % scores.mean() 

    # Cs = np.linspace(21, 24, 250)
    # gammas = np.logspace(-2, 0, 10)

    # Cs = np.linspace(4.0, 4.5, 100)  # Best C: 4.3838383838383841
    # gammas = np.linspace(0.05, 0.07, 100)  # Best Gamma: 0.063333333333333339

    clf = GridSearchCV(estimator=svc, param_grid=dict(average=np.arange(1,100, 1)),
                       cv=cross_validation.ShuffleSplit(2025, n_iter=30, test_size=0.05),
                       n_jobs=6)
    clf.fit(X, y)

    print clf.best_score_
    print clf.best_estimator_

    # Prediction
    T_ids = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=0).astype('int')
    T = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=range(1, 8))

    # Normalize
    T = attempt2(T)
    T_pred = clf.predict(T)

    with open('prediction_lennart.csv', 'w') as fp:
        a = csv.writer(fp, delimiter=',')
        rows = zip(T_ids, T_pred.astype('int'))
        a.writerows([['Id', 'Label']])
        a.writerows(rows)


if __name__ == "__main__":
    main()