#__author__ = 'lennart'
from __future__ import division
import csv
import numpy as np
import math
import random
import time
import datetime
from sknn.mlp import Classifier, Layer
from sklearn import svm, grid_search




def CV10(C,X,y):
    '''
    Splits the data into 10 buckets of equal size (except for the last bucket which might be longer if sample_size % 10 != 0)
    '''

    results = []

    sample_size = int(X.shape[0])
    # define train and test samples sizes
    train_block_size = int(sample_size/10)
    test_block_size = sample_size - train_block_size*9

    # Performing the CV 10 times

    for i in range (0,10):
        a = i * train_block_size
        if i==9:
            b = a + test_block_size
        else:
            b = a + train_block_size

        # Split data

        X_test = X[a:b,:]
        y_test = y[a:b]

        X_train = np.concatenate((X[0:a,:], X[b:,:]),0)
        y_train = np.concatenate((y[0:a], y[b:]),0)




        # Test different models here

<<<<<<< HEAD
        #clf = svm.SVC(C=C, kernel='rbf', shrinking=True)
        clf = svm.LinearSVC()
        
=======
        parameters = {'kernel':('linear', 'rbf'), 'C':[0.1, 10], 'gamma':[0.00001, 500]}

        # clf = Classifier(
        #     layers=[
        #     Layer("Tanh", units=100),
        #     Layer("Sigmoid")],
        #     learning_rate=0.001,
        #     n_iter=25)
        
        svr = svm.SVC(probability=True)
        clf = grid_search.GridSearchCV(svr, parameters)
>>>>>>> 52f6ab3cde3ab296e9e71101ef86fe86a86b5b45
        clf.fit(X_train,y_train)

        #print clf.predict_proba(X_train)

        y_pred = clf.predict(X_test)

        # End model test

        result = computeAccuracy(i, y_test, y_pred)
        results.append(result)

    meanResult = np.mean(results)
    varResults = np.var(results)

    results = []




    print 'Mean results : %.2f' % (meanResult*100)
    print 'Variance results : %.5f' % varResults
    return meanResult

    #print rmse_CV


def computeAccuracy(test, y_test, y_pred):
    nbCorrectClassification = 0
    for i in range (0, y_test.size):
        if y_test[i] == y_pred[i]:
            nbCorrectClassification += 1
    #print ' (%d) Correctly classified : %.2f' % (test, (nbCorrectClassification / y_test.size)*100)
    return nbCorrectClassification / y_test.size


###############################
# Random Transformations
###############################

# Log on first featurem sqrt on all the others
def fastForward(X,y, train, gen_indices):
    '''
    fastForward
    ----------
    -  TODO : 
    ----------
           - Add C testing after feature brute force --DONE--
           - Output the best feature transformation indices to avoid recomputing every time --DONE-- 
           - Put a condition on the number of feature we keep --DONE--
                - E.g. if after 15 features the mean does not  improve, do not add more features in the star set
           - Try different classifiers. 
    '''

    # Init rmse_CV

    for i in range(0,7):
        inv = (1.0/X[:,i]).reshape(X.shape[0],1)
        invLog = (1.0/np.log10(1+X[:,i])).reshape(X.shape[0],1)
        log = np.log10(1.0+X[:,i]).reshape(X.shape[0],1)
        root = np.sqrt(X[:,i]).reshape(X.shape[0],1)
        square = np.multiply(X[:,i], X[:,i]).reshape(X.shape[0],1)
        cube = (X[:,i]*X[:,i]*X[:,i]).reshape(X.shape[0],1)
        
        print 'Appending feature transformations for initial feature no %d' % (i+1)
        X = np.append(X,inv, axis=1)
        X = np.append(X,log, axis=1)
        X = np.append(X,invLog, axis=1)
        X = np.append(X,root, axis=1)
        X = np.append(X,square, axis=1)
        X = np.append(X,cube, axis=1)
    print X.shape


    # print 'Appending multiplication among features'
    # X_temp = X
    # cnt = 0
    # for i in range(0, X.shape[1]):
        # for j in range (i+1, X.shape[1]):
            # X_temp = np.append(X_temp,(X[:,i]*X[:,j]).reshape(X.shape[0],1), axis=1)
            # cnt += 1
    # print '%d new features added for multiplications' % cnt
    # print X_temp.shape

    # print 'Appending division among features'
    # cnt = 0
    # for i in range(0, X.shape[1]):
        # for j in range (0, X.shape[1]):
            # if(i != j):
                # X_temp = np.append(X_temp,np.divide(X[:,i],X[:,j]).reshape(X.shape[0],1), axis=1)
                # cnt += 1
    # print '%d new features added for divisions' % cnt
    # print X_temp.shape

    # X = X_temp

    print 'Computing mean and std for normalization'
    # array of feature means
    mu = np.mean(X,axis=0)
    # array of feature std deviations
    sigma = np.std(X,axis=0)
    
    print 'Normalizing'
    # Normalize
    for col in range (0,X.shape[1]):
        # print 'col=%d' % col
        feat_normed = (X[:,col] - mu[col]) / sigma[col]
        X[:,col] = feat_normed

    print 'Test if nan values among features'
    print X[np.isnan(X) == True] != 0
   

    # NOTE : Variance is not used here... Maybe we can find a way to make use of it.
    maxMean = 0.0
    #minVar = 99999999.0
    bestC = -1
    improved = False
    if train:
        star_indices = [0]
        bestIndex = 0
        # Append intercept here
        X_star = np.empty(shape=(X.shape[0],0))
        X_star = np.insert(X_star,0,1, axis=1)
    
        print 'Fast Forward Algorithm start'
        print X.shape # ---------> changed to 23 here  (flo)
        for j in range(1, 10):
            improved = False
            for i in range(1, X.shape[1]):
                if i not in star_indices:
                    print 'j=%d' % j
                    print 'i=%d' % i

                    # Feature test set
                    X_test = np.c_[X_star, X[:,i]]

                    # Cross validation test
                    mean = CV10(1.0, X_test,y)

                    # If mean increases save index and new mean
                    if maxMean < mean:
                        improved = True
                        bestIndex = i
                        maxMean = mean
                        print '-------------------> New best mean : %.5f' % maxMean

            # Append optimal feature to star feature set and optimal index to star index set
            # Only if mean has improved for the last `j`
            if improved is True:
                X_star = np.c_[X_star, X[:,bestIndex]]
                star_indices = np.r_[star_indices, bestIndex]

        # Find best C :
<<<<<<< HEAD
        print 'Test for C'
        for C in np.arange(0.1, 3.1, 0.1):
            print 'C=%.1f' % C
=======
        # for C in [0.1, 3.0, 0.1]: (Floran)
        for C in [0.1, 0.15, 0.2]:
>>>>>>> 52f6ab3cde3ab296e9e71101ef86fe86a86b5b45
            mean = CV10(C, X_star,y)
            if maxMean < mean:
                bestC = C
                maxMean = mean
                print '--------------------> New best mean : %.5f' % maxMean
                print 'New optimal C %.2f' % bestC
            

        print 'Best mean is %.5f' % maxMean
        print 'Optimal C is %.1f' % bestC
        print 'Optimal feature indices are :'
        print star_indices
        return (X_star, star_indices, bestC)
    else:
        return X[:,gen_indices.astype(int)]




def compromise():
    T_ids1 = np.genfromtxt('prediction_flo.csv', delimiter=',', usecols=0).astype('int')
    T1 = np.genfromtxt('prediction_flo.csv', delimiter=',', usecols=1).astype('int')
    
    T_ids2 = np.genfromtxt('prediction_flo_a3.csv', delimiter=',', usecols=0).astype('int')
    T2 = np.genfromtxt('prediction_flo_a3.csv', delimiter=',', usecols=1).astype('int')

    T_ids3 = np.genfromtxt('prediction_flo_a4.csv', delimiter=',', usecols=0).astype('int')
    T3 = np.genfromtxt('prediction_flo_a4.csv', delimiter=',', usecols=1).astype('int')
    cnt = 0
    print T_ids2
    with open('prediction_flo_comp.csv', 'w') as fp:
        a = csv.writer(fp, delimiter=',')

        a.writerows([['Id', 'Label']])

        for i in range(1, T2.shape[0]):
            print 'T_ids = %d'% T_ids1[i]
            cnt0=0
            cnt1=0
            cnt2=0
            if T1[i] == 0:
                cnt0+=1
            elif T1[i] == 1:
                cnt1+=1
            else:
                cnt2+=1

            if T2[i] == 0:
                cnt0+=1
            elif T2[i] == 1:
                cnt1+=1
            else:
                cnt2+=1

            if T3[i] == 0:
                cnt0+=1
            elif T3[i] == 1:
                cnt1+=1
            else:
                cnt2+=1
            
            print 'cnt0=%d, cnt1=%d, cnt2=%d' % (cnt0, cnt1, cnt2)
            if cnt0>=2:
                a.writerows([[T_ids1[i], '0']])
            elif cnt1>=2:
                a.writerows([[T_ids1[i], '1']])
            elif cnt2>=2:
                a.writerows([[T_ids1[i], '2']])
            else:
                a.writerows([[T_ids1[i], '?']])

        # # # # # Write test prediction
   
        


def attempt1(X,y):

    feature1= 1.0/np.log10(1+X[:,0])
    X[:,0] = feature1

    feature2 = np.log10(1+X[:,1])
    X[:,1] = feature2

    feature3 = np.sqrt(X[:,2])
    X[:,2] = feature3


    feature5 = 1.0/X[:,4]
    X[:,4] = feature5

    # feature1 = np.sqrt(X[:,0])
    # X[:,0] = feature1

    feature7 = np.sqrt(X[:,6])
    X[:,6] = feature7

    combi1 = np.multiply(feature1,feature5).reshape(X.shape[0],1)
    combi2 = np.multiply(feature1,feature7).reshape(X.shape[0],1)
    combi3 = np.multiply(feature3,feature7).reshape(X.shape[0],1)
    combi4 = np.multiply(feature7,feature7).reshape(X.shape[0],1)
    combi5 = np.multiply(feature3,feature3).reshape(X.shape[0],1)

    X = np.append(X, combi1, axis=1)
    X = np.append(X, combi2, axis=1)
    X = np.append(X, combi3, axis=1)
    X = np.append(X, combi4, axis=1)
    X = np.append(X, combi5, axis=1)
    
    mu_feat = np.mean(X, axis=0)
    sigma_feat = np.std(X, axis=0)

    for i in range (0, X.shape[1]):
        X[:,i] = (X[:,i]-mu_feat[i])/sigma_feat[i]

<<<<<<< HEAD
    #CV10(1.0, X,y)
=======
    #CV10(1.0,X,y)
>>>>>>> 52f6ab3cde3ab296e9e71101ef86fe86a86b5b45
    return X

def remove1Feature(X,y):
    ## Normalization of the data

    for f in range (0,X.shape[1]):
        print 'Removing feature %s' % `f+1`
        X_tild = np.delete(X, f, 1)

        mu_feat = np.mean(X_tild, axis=0)
        sigma_feat = np.std(X_tild, axis=0)

        for i in range (0, X_tild.shape[1]):
            X_tild[:,i] = (X_tild[:,i]-mu_feat[i])/sigma_feat[i]

        CV10(X_tild,y)


################################

def main():


    compromise()
    # train = True

    # # # Read training CSV
    # X = np.genfromtxt('data/train.csv', delimiter=',', usecols=range(1, 8))
    # y = np.genfromtxt('data/train.csv', delimiter=',', usecols=8)

<<<<<<< HEAD
    # #attempt1(X,y)
=======
    X_star = attempt1(X,y)
>>>>>>> 52f6ab3cde3ab296e9e71101ef86fe86a86b5b45
    # [X_star, star_indices, bestC] = fastForward(X,y,train,None)
    # star_indices_txt = ' '.join(map(str, star_indices))
    # now = time.time()
    # st = datetime.datetime.fromtimestamp(now).strftime('%d-%m-%Y %H:%M:%S')
<<<<<<< HEAD

    # with open('optimal_feature_indices.txt', 'a') as fp:
            # fp.write(st)
            # fp.write('\n')
            # fp.write(star_indices_txt)
            # fp.write('\n')
            # fp.close()
=======

    # with open('optimal_feature_indices.txt', 'a') as fp:
    #         fp.write(st)
    #         fp.write('\n')
    #         fp.write(star_indices_txt)
    #         fp.write('\n')
    #         fp.close()

    parameters = {'kernel':('linear', 'rbf'), 'C':[0.1, 10], 'gamma':[0.00001, 500]}

    svr = svm.SVC(probability=True)
    clf = grid_search.GridSearchCV(svr, parameters)
    clf.fit(X_star,y)

        #print clf.predict_proba(X_train)

    
>>>>>>> 52f6ab3cde3ab296e9e71101ef86fe86a86b5b45

    # X_star = fastForward(X,y,False,np.array(X_star_ind))
    # X_star = attempt1(X,y)
    

    # #clf = svm.SVC(C=1.0, kernel='rbf')
    # clf = svm.LinearSVC()
    # clf.fit(X_star,y)

<<<<<<< HEAD
    # # # # # # # Read test CSV
    # T_ids = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=0).astype('int')
    # T = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=range(1, 8))

    # # # # T = fastForward(T,y,False,star_indices)
    # T = attempt1(T,y)
=======
    # # # Read test CSV
    T_ids = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=0).astype('int')
    T = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=range(1, 8))

    T = attempt1(T,y)


    y_pred = clf.predict(T)
    
>>>>>>> 52f6ab3cde3ab296e9e71101ef86fe86a86b5b45
    # pred = clf.predict(T)

    # # # # # Write test prediction
    # with open('prediction_flo_a4.csv', 'w') as fp:
        # a = csv.writer(fp, delimiter=',')
        # rows = zip(T_ids, pred.astype('int'))
        # a.writerows([['Id', 'Label']])
        # a.writerows(rows)


if __name__ == "__main__":
    main()