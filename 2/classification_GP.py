#__author__ = 'lennart'
from __future__ import division
import csv
import numpy as np
import math
import random
import time
import datetime
from sklearn import svm, grid_search
from sklearn.cross_validation import cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn import cross_validation
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.gaussian_process import GaussianProcess

# class init:
    # def __init__(self, est):
        # self.est = est
    # def predict(self, X):
        # return self.est.predict_proba(X)[:,1][:,numpy.newaxis]
    # def fit(self, X, y):
        # self.est.fit(X, y)


def CV10(X,y):
    '''
    Splits the data into 10 buckets of equal size (except for the last bucket which might be longer if sample_size % 10 != 0)
    '''

    results = []

    sample_size = int(X.shape[0])
    # define train and test samples sizes
    train_block_size = int(sample_size/10)
    test_block_size = sample_size - train_block_size*9

    # Performing the CV 10 times

    for i in range (0,10):
        a = i * train_block_size
        if i==9:
            b = a + test_block_size
        else:
            b = a + train_block_size

        # Split data

        X_test = X[a:b,:]
        y_test = y[a:b]

        X_train = np.concatenate((X[0:a,:], X[b:,:]),0)
        y_train = np.concatenate((y[0:a], y[b:]),0)


        C = np.arange(0.1, 5.1, 0.5)
        gamma = np.arange(0.00001, 0.4, 0.1)

        # Test different models here

        parameters_bc = {'max_samples':[0.7, 0.8, 0.9, 1.0], 'max_features':[0.5, 0.6, 0.7, 0.8, 0.9, 1.0]}
        parameters_svc = {'C':C, 'gamma':gamma}

        svc_c = svm.SVC()
        svc_clf = grid_search.GridSearchCV(svc_c, parameters_svc)
        svc_clf.fit(X_train, y_train)

        print 'Best params for SVC :'
        print svc_clf.best_params_
        

        bc = BaggingClassifier(svm.SVC(C=svc_clf.best_params_['C'], gamma=svc_clf.best_params_['gamma']))
        clf = grid_search.GridSearchCV(bc, parameters_bc)


        # This function does crossvalidation, but here it does cross-validation over 'our' cross validation.
        # Can be useful when we don't use our CV.
        # --->
        # scores = cross_val_score(clf, X_train, y_train)
        #print 'Score mean : %.5f' % scores.mean() 

        clf.fit(X_train,y_train)

        print 'Best params for Bagging :'
        print clf.best_params_

        y_pred = clf.predict(X_test)

        # End model test

        result = computeAccuracy(i, y_test, y_pred)
        results.append(result)

    meanResult = np.mean(results)
    varResults = np.var(results)

    results = []




    print 'Mean results : %.2f' % (meanResult*100)
    print 'Variance results : %.5f' % varResults
    return meanResult

    #print rmse_CV


def computeAccuracy(test, y_test, y_pred):
    nbCorrectClassification = 0
    for i in range (0, y_test.size):
        if y_test[i] == y_pred[i]:
            nbCorrectClassification += 1
    print ' (%d) Correctly classified : %.2f' % (test, (nbCorrectClassification / y_test.size)*100)
    return nbCorrectClassification / y_test.size


###############################
# Prediction
###############################
def prediction_RFC(X,y,T):

    

    class_weight = {0:1, 1:1, 2:3}
    max_features=np.linspace(0.1, 1.0, 100)
    min_samples_split=np.arange(2,10,1)
    min_samples_leaf=np.arange(1,10,1)
    
    parameters_rfc = {'max_features':max_features, 'min_samples_split':min_samples_split, 'min_samples_leaf':min_samples_leaf}
    
    rfc = RandomForestClassifier(n_estimators=50, bootstrap=True, oob_score=True, n_jobs=-1, class_weight=class_weight)
    
    clf = grid_search.GridSearchCV(rfc, parameters_rfc)
    clf.fit(X, y)
    print 'Best params for RBM :'
    print clf.best_params_
    
    # Crossvalidation score (quite similar to our implementation)
    scores = cross_val_score(clf, X, y, cv=10)
    print 'Score mean : %.5f' % scores.mean() 

    y_pred = clf.predict(T)

    return y_pred
    
def prediction_GP(X,y,T):

    
    theta0 = np.linspace(1e-2, 1, 20)
    
    
    parameters_gp = {'theta0':theta0}
    
    gp = GaussianProcess(theta0=0.68736842105263163, 
        thetaL=1e-4, 
        thetaU=1e-1,
        corr='squared_exponential')
    gp.fit(X,y)
    # clf = grid_search.GridSearchCV(gp, parameters_gp)
    # clf.fit(X, y)
    # print 'Best params for RBM :'
    # print clf.best_params_
    
    # Crossvalidation score (quite similar to our implementation)
    scores = cross_val_score(gp, X, y, cv=10)
    print 'Score mean : %.5f' % scores.mean() 

    y_pred = gp.predict(T)

    return y_pred
    
def prediction_gradientBoost(X,y,T):

    

    class_weight = {0:1, 1:1, 2:3}
    max_features=np.linspace(0.1, 1.0, 100)
    min_samples_split=np.arange(2,10,1)
    min_samples_leaf=np.arange(1,10,1)
    learning_rate=np.linspace(0.001, 1, 50)
    subsample=np.linspace(0.1,1.0,20)
    
    parameters_gbc = {'learning_rate':learning_rate, 'max_features':max_features, 'min_samples_split':min_samples_split, 'min_samples_leaf':min_samples_leaf}

    dtc = LogisticRegression(class_weight=class_weight, solver='newton-cg', C=3.0408163265)

    gbc = GradientBoostingClassifier(init=dtc, loss='deviance')
    
    clf = grid_search.GridSearchCV(gbc, parameters_gbc)
    clf.fit(X, y)
    print 'Best params for RBM :'
    print clf.best_params_
    
    # Crossvalidation score (quite similar to our implementation)
    scores = cross_val_score(clf, X, y, cv=10)
    print 'Score mean : %.5f' % scores.mean() 

    y_pred = clf.predict(T)

    return y_pred

###############################
# Feature Transformations
###############################
def attempt2(X):

    feature1= 1.0/np.log10(1+X[:,0])
    X[:,0] = feature1

    feature2 = np.sqrt(X[:,1])
    X[:,1] = feature2

    feature3 = np.sqrt(X[:,2])
    X[:,2] = feature3

    feature4 = 1.0/np.log10(1+X[:,3])
    X[:,3] = feature4

    feature5 = 1.0/X[:,4]
    X[:,4] = feature5

    feature6 = X[:,5]

    feature7 = np.sqrt(X[:,6])
    X[:,6] = feature7

    combi1 = np.multiply(feature1,feature5).reshape(X.shape[0],1)
    combi2 = np.multiply(feature1,feature2).reshape(X.shape[0],1)
    combi3 = np.multiply(feature1,feature3).reshape(X.shape[0],1)
    combi4 = np.multiply(feature1,feature4).reshape(X.shape[0],1)
    combi5 = np.multiply(feature1,feature7).reshape(X.shape[0],1)
    combi6 = np.multiply(feature2,feature3).reshape(X.shape[0],1)
    combi7 = np.divide(feature1,feature5).reshape(X.shape[0],1)
    # combi8 = np.divide(feature1,feature2).reshape(X.shape[0],1)
    combi9 = np.divide(feature2,feature3).reshape(X.shape[0],1)
    combi10 = np.divide(feature4,feature2).reshape(X.shape[0],1)
    # combi11 = np.divide(feature5,feature6).reshape(X.shape[0],1)
    # combi12 = np.divide(feature1,feature5).reshape(X.shape[0],1)

    X = np.append(X, combi1, axis=1)
    X = np.append(X, combi2, axis=1)
    X = np.append(X, combi3, axis=1)
    X = np.append(X, combi4, axis=1)
    X = np.append(X, combi5, axis=1)
    X = np.append(X, combi6, axis=1)
    X = np.append(X, combi7, axis=1)
    # X = np.append(X, combi8, axis=1)
    X = np.append(X, combi9, axis=1)
    X = np.append(X, combi10, axis=1)
    # X = np.append(X, combi11, axis=1)
    # X = np.append(X, combi12, axis=1)

    mu_feat = np.mean(X, axis=0)
    sigma_feat = np.std(X, axis=0)

    for i in range (1, X.shape[1]):
        X[:,i] = (X[:,i]-mu_feat[i])/sigma_feat[i]

    return X
    
def attempt1(X):

    feature1= 1.0/np.log10(1+X[:,0])
    X[:,0] = feature1

    feature2 = np.log10(1+X[:,1])
    X[:,1] = feature2

    feature3 = np.sqrt(X[:,2])
    X[:,2] = feature3


    feature5 = 1.0/X[:,4]
    X[:,4] = feature5

    # feature1 = np.sqrt(X[:,0])
    # X[:,0] = feature1

    feature7 = np.sqrt(X[:,6])
    X[:,6] = feature7

    combi1 = np.multiply(feature1,feature5).reshape(X.shape[0],1)

    X = np.append(X, combi1, axis=1)

    mu_feat = np.mean(X, axis=0)
    sigma_feat = np.std(X, axis=0)

    for i in range (0, X.shape[1]):
        X[:,i] = (X[:,i]-mu_feat[i])/sigma_feat[i]

    return X

def remove1Feature(X,y):
    ## Normalization of the data

    for f in range (0,X.shape[1]):
        print 'Removing feature %s' % `f+1`
        X_tild = np.delete(X, f, 1)

        mu_feat = np.mean(X_tild, axis=0)
        sigma_feat = np.std(X_tild, axis=0)

        for i in range (0, X_tild.shape[1]):
            X_tild[:,i] = (X_tild[:,i]-mu_feat[i])/sigma_feat[i]

        CV10(X_tild,y)


################################



def main():

    train = True

    # Read training CSV
    X = np.genfromtxt('data/train.csv', delimiter=',', usecols=range(1, 8))
    y = np.genfromtxt('data/train.csv', delimiter=',', usecols=8)

    # # # Read test CSV
    T_ids = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=0).astype('int')
    T = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=range(1, 8))

    X_transf = attempt2(X)

    T_transf = attempt2(T)

    y_pred = prediction_GP(X_transf, y, T_transf)

   
        # # Write test prediction
    with open('prediction_RFC.csv', 'w') as fp:
        a = csv.writer(fp, delimiter=',')
        rows = zip(T_ids, y_pred.astype('int'))
        a.writerows([['Id', 'Label']])
        a.writerows(rows)


if __name__ == "__main__":
    main()