__author__ = 'lennart'
import csv
import numpy as np
from sklearn import preprocessing
from sklearn.svm import SVC
from sklearn.grid_search import GridSearchCV
import sklearn.cross_validation as skcv
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn import cross_validation
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis



def attempt2(X):

    feature1= 1.0/np.log10(1+X[:,0])
    X[:,0] = feature1

    feature2 = np.sqrt(X[:,1])
    X[:,1] = feature2

    feature3 = np.sqrt(X[:,2])
    X[:,2] = feature3

    feature4 = 1.0/np.log10(1+X[:,3])
    X[:,3] = feature4

    feature5 = 1.0/X[:,4]
    X[:,4] = feature5

    feature6 = X[:,5]

    feature7 = np.sqrt(X[:,6])
    X[:,6] = feature7

    combi1 = np.multiply(feature1,feature5).reshape(X.shape[0],1)
    combi2 = np.multiply(feature1,feature2).reshape(X.shape[0],1)
    combi3 = np.multiply(feature1,feature3).reshape(X.shape[0],1)
    combi4 = np.multiply(feature1,feature4).reshape(X.shape[0],1)
    combi5 = np.multiply(feature1,feature7).reshape(X.shape[0],1)
    combi6 = np.multiply(feature2,feature3).reshape(X.shape[0],1)
    combi7 = np.divide(feature1,feature5).reshape(X.shape[0],1)
    # combi8 = np.divide(feature1,feature2).reshape(X.shape[0],1)
    combi9 = np.divide(feature2,feature3).reshape(X.shape[0],1)
    combi10 = np.divide(feature4,feature2).reshape(X.shape[0],1)
    # combi11 = np.divide(feature5,feature6).reshape(X.shape[0],1)
    # combi12 = np.divide(feature1,feature5).reshape(X.shape[0],1)

    X = np.append(X, combi1, axis=1)
    X = np.append(X, combi2, axis=1)
    X = np.append(X, combi3, axis=1)
    X = np.append(X, combi4, axis=1)
    X = np.append(X, combi5, axis=1)
    X = np.append(X, combi6, axis=1)
    X = np.append(X, combi7, axis=1)
    # X = np.append(X, combi8, axis=1)
    X = np.append(X, combi9, axis=1)
    X = np.append(X, combi10, axis=1)
    # X = np.append(X, combi11, axis=1)
    # X = np.append(X, combi12, axis=1)

    mu_feat = np.mean(X, axis=0)
    sigma_feat = np.std(X, axis=0)

    for i in range (1, X.shape[1]):
        X[:,i] = (X[:,i]-mu_feat[i])/sigma_feat[i]

    return X

def attempt1(X):
    feature1 = 1.0 / np.log10(1 + X[:, 0])
    X[:, 0] = feature1

    feature2 = np.log10(1 + X[:, 1])
    X[:, 1] = feature2

    feature3 = np.sqrt(X[:, 2])
    X[:, 2] = feature3

    feature5 = 1.0 / X[:, 4]
    X[:, 4] = feature5

    feature7 = np.sqrt(X[:, 6])
    X[:, 6] = feature7

    combi1 = np.multiply(feature1, feature5).reshape(X.shape[0], 1)

    X = np.append(X, combi1, axis=1)

    mu_feat = np.mean(X, axis=0)
    sigma_feat = np.std(X, axis=0)

    for i in range(0, X.shape[1]):
        X[:, i] = (X[:, i] - mu_feat[i]) / sigma_feat[i]

    return X


def main():
    # Read training CSV
    X = np.genfromtxt('data/train.csv', delimiter=',', usecols=range(1, 8))
    y = np.genfromtxt('data/train.csv', delimiter=',', usecols=8)


    # Standardize data
    # X_train, X_test, y_train, y_test = skcv.train_test_split(X, y, train_size=0.9)
    
    X_train = attempt2(X)
    # X_test = attempt1(X_test)

    # X_train = X

    lda = LinearDiscriminantAnalysis(n_components=2, solver='eigen')
    X_train = lda.fit_transform(X_train,y)
    # X_test = lda.transform(X_test)

    knc = KNeighborsClassifier(n_neighbors=25, metric='minkowski')
    # knc.fit(X_train, y_train)

    clf = GridSearchCV(
        estimator=knc, 
        param_grid=dict(
            weights=['uniform','distance'],
            algorithm=['kd_tree','ball_tree'],
            p=[1,2,3]
            ),
        cv=skcv.ShuffleSplit(2025, n_iter=50, test_size=0.1),
        n_jobs=-1)


    lda = LinearDiscriminantAnalysis(n_components=2, solver='eigen')

    print "Starting LDA..."
    X = lda.fit_transform(X, y)
    print X.shape
    print "LDA done"

    # Grid search using SVM & CV
    svc = SVC(kernel='rbf')

    Cs = np.linspace(4.0, 4.5, 20)  # Best C: 4.3838383838383841
    gammas = np.linspace(0.05, 0.07, 20)  # Best Gamma: 0.063333333333333339




    # Prediction
    T_ids = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=0).astype('int')
    T = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=range(1, 8))

    # Normalize
    T = attempt2(T)
    T = lda.transform(T)

    T = lda.transform(T)

    T_pred = clf.predict(T)

    with open('prediction_lda_kd.csv', 'w') as fp:
        a = csv.writer(fp, delimiter=',')
        rows = zip(T_ids, T_pred.astype('int'))
        a.writerows([['Id', 'Label']])
        a.writerows(rows)


if __name__ == "__main__":
    main()