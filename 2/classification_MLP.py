#__author__ = 'lennart'
from __future__ import division
import csv
import numpy as np
import math
import random
import time
import datetime
from sknn.mlp import Classifier, Layer
from sklearn import svm, grid_search
from sklearn.cross_validation import cross_val_score
from sklearn import cross_validation




def CV10(X,y):
    '''
    Splits the data into 10 buckets of equal size (except for the last bucket which might be longer if sample_size % 10 != 0)
    '''

    results = []

    sample_size = int(X.shape[0])
    # define train and test samples sizes
    train_block_size = int(sample_size/10)
    test_block_size = sample_size - train_block_size*9

    # Performing the CV 10 times

    for i in range (0,10):
        a = i * train_block_size
        if i==9:
            b = a + test_block_size
        else:
            b = a + train_block_size

        # Split data

        X_test = X[a:b,:]
        y_test = y[a:b]

        X_train = np.concatenate((X[0:a,:], X[b:,:]),0)
        y_train = np.concatenate((y[0:a], y[b:]),0)


        C = np.arange(0.1, 5.1, 0.5)
        gamma = np.arange(0.00001, 0.4, 0.1)

        # Test different models here

        parameters_bc = {'max_samples':[0.7, 0.8, 0.9, 1.0], 'max_features':[0.5, 0.6, 0.7, 0.8, 0.9, 1.0]}
        parameters_svc = {'C':C, 'gamma':gamma}

        svc_c = svm.SVC()
        svc_clf = grid_search.GridSearchCV(svc_c, parameters_svc)
        svc_clf.fit(X_train, y_train)

        print 'Best params for SVC :'
        print svc_clf.best_params_
        
        adaboost = AdaBoostClassifier(
            base_estimator=svm.SVC(C=svc_clf.best_params_['C'], gamma=svc_clf.best_params_['gamma']),
            n_estimators=100,
            algorithm='SAMME.R')



        # This function does crossvalidation, but here it does cross-validation over 'our' cross validation.
        # Can be useful when we don't use our CV.
        # --->
        # scores = cross_val_score(clf, X_train, y_train)
        #print 'Score mean : %.5f' % scores.mean() 

        adaboost.fit(X_train,y_train)

        print 'Boosting score :'
        print adaboost.score(X_train, y_train)

        y_pred = adaboost.predict(X_test)

        # End model test
        return y_pred



###############################
# Prediction
###############################

def prediction(X,y,T):

    parameters_nn = {'learning_rate':np.linspace(0.001, 0.01, 10), 'n_iter':np.arange(25, 50, 1)}

    clf = grid_search.GridSearchCV(Classifier(
        layers=[
            Layer("Tanh", units=128),
            Layer("Maxout", units=128, pieces=3),
            Layer("Sigmoid", units=64),
            Layer("Softmax")
            ]), parameters_nn)
    # 

    clf.fit(X,y)
    print 'Best params for DTC :'
    print clf.best_params_


    nn = Classifier(
        layers=[
            Layer("Tanh", units=128),
            Layer("Maxout", units=128, pieces=3),
            Layer("Sigmoid", units=64),
            Layer("Softmax")
            ],
        learning_rate=clf.best_params_['learning_rate'],
        n_iter=clf.best_params_['n_iter'])
    nn.fit(X, y)



    scores = cross_val_score(nn, X, y, cv=10)
    print 'Score mean : %.5f' % scores.mean() 


    y_pred = nn.predict(T)

    # End model test
    return y_pred


class init:
    def __init__(self, est):
        self.est = est
    def predict(self, X):
        return self.est.predict_proba(X)[:,1][:,numpy.newaxis]
    def fit(self, X, y, sample_weight=None):
        self.est.fit(X, y)

###############################
# Feature Transformations
###############################

def attempt1(X):

    feature1= 1.0/np.log10(1+X[:,0])
    X[:,0] = feature1

    # feature2 = np.log10(1+X[:,1])
    # X[:,1] = feature2

    feature2 = np.sqrt(X[:,1])
    X[:,1] = feature2

    feature3 = np.sqrt(X[:,2])
    X[:,2] = feature3


    feature5 = 1.0/X[:,4]
    X[:,4] = feature5

    # feature1 = np.sqrt(X[:,0])
    # X[:,0] = feature1

    feature7 = np.sqrt(X[:,6])
    X[:,6] = feature7

    # combi1 = np.multiply(feature1,feature5).reshape(X.shape[0],1)
    combi2 = np.multiply(feature1,feature2).reshape(X.shape[0],1)

    # X = np.append(X, combi1, axis=1)
    X = np.append(X, combi2, axis=1)

    mu_feat = np.mean(X, axis=0)
    sigma_feat = np.std(X, axis=0)

    for i in range (1, X.shape[1]):
        X[:,i] = (X[:,i]-mu_feat[i])/sigma_feat[i]

    return X

def remove1Feature(X,y):
    ## Normalization of the data

    for f in range (0,X.shape[1]):
        print 'Removing feature %s' % `f+1`
        X_tild = np.delete(X, f, 1)

        mu_feat = np.mean(X_tild, axis=0)
        sigma_feat = np.std(X_tild, axis=0)

        for i in range (0, X_tild.shape[1]):
            X_tild[:,i] = (X_tild[:,i]-mu_feat[i])/sigma_feat[i]

        CV10(X_tild,y)


################################

def main():

    train = True

    # Read training CSV
    X = np.genfromtxt('data/train.csv', delimiter=',', usecols=range(1, 8))
    y = np.genfromtxt('data/train.csv', delimiter=',', usecols=8)

    # # # Read test CSV
    T_ids = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=0).astype('int')
    T = np.genfromtxt('data/validate_and_test.csv', delimiter=',', usecols=range(1, 8))

    X_transf = attempt1(X)

    T_transf = attempt1(T)

    y_pred = prediction(X_transf, y, T_transf)

   
     # # Write test prediction
    with open('prediction_MLP.csv', 'w') as fp:
        a = csv.writer(fp, delimiter=',')
        rows = zip(T_ids, y_pred.astype('int'))
        a.writerows([['Id', 'Label']])
        a.writerows(rows)


if __name__ == "__main__":
    main()