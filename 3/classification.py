#__author__ = 'lennart'
from __future__ import division
import csv
import numpy as np
import math
import random
import time
import datetime
from sklearn import svm, grid_search
from sklearn.cross_validation import cross_val_score
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, BaggingClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn import cross_validation
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
import theano
from theano import tensor as T
import numpy as np
from load import mnist
from foxhound.utils.vis import grayscale_grid_vis, unit_scale
from scipy.misc import imsave


#
# -------------------------PROJECT 3:  THE IDEA :
#  - Since we have two sets of features : find a way to apply a different kernel on both of them, then SVM.
#  ----> Problem : SVM only accepts one kernel. Maybe there is a way around
#  ----> The sum of two kernels is another kernel. Maybe apply the sum of both kernels to all data ?
#
#

def CV10(X,y):
    '''
    Splits the data into 10 buckets of equal size (except for the last bucket which might be longer if sample_size % 10 != 0)
    '''

    results = []

    sample_size = int(X.shape[0])
    # define train and test samples sizes
    train_block_size = int(sample_size/10)
    test_block_size = sample_size - train_block_size*9

    # Performing the CV 10 times

    for i in range (0,10):
        a = i * train_block_size
        if i==9:
            b = a + test_block_size
        else:
            b = a + train_block_size

        # Split data

        X_test = X[a:b,:]
        y_test = y[a:b]

        X_train = np.concatenate((X[0:a,:], X[b:,:]),0)
        y_train = np.concatenate((y[0:a], y[b:]),0)


        C = np.arange(0.1, 5.1, 0.5)
        gamma = np.arange(0.00001, 0.4, 0.1)

        # Test different models here

        parameters_bc = {'max_samples':[0.7, 0.8, 0.9, 1.0], 'max_features':[0.5, 0.6, 0.7, 0.8, 0.9, 1.0]}
        parameters_svc = {'C':C, 'gamma':gamma}

        svc_c = svm.SVC()
        svc_clf = grid_search.GridSearchCV(svc_c, parameters_svc)
        svc_clf.fit(X_train, y_train)

        print 'Best params for SVC :'
        print svc_clf.best_params_
        

        bc = BaggingClassifier(svm.SVC(C=svc_clf.best_params_['C'], gamma=svc_clf.best_params_['gamma']))
        clf = grid_search.GridSearchCV(bc, parameters_bc)


        # This function does crossvalidation, but here it does cross-validation over 'our' cross validation.
        # Can be useful when we don't use our CV.
        # --->
        # scores = cross_val_score(clf, X_train, y_train)
        #print 'Score mean : %.5f' % scores.mean() 

        clf.fit(X_train,y_train)

        print 'Best params for Bagging :'
        print clf.best_params_

        y_pred = clf.predict(X_test)

        # End model test

        result = computeAccuracy(i, y_test, y_pred)
        results.append(result)

    meanResult = np.mean(results)
    varResults = np.var(results)

    results = []




    print 'Mean results : %.2f' % (meanResult*100)
    print 'Variance results : %.5f' % varResults
    return meanResult

    #print rmse_CV


def computeAccuracy(test, y_test, y_pred):
    nbCorrectClassification = 0
    for i in range (0, y_test.size):
        if y_test[i] == y_pred[i]:
            nbCorrectClassification += 1
    print ' (%d) Correctly classified : %.2f' % (test, (nbCorrectClassification / y_test.size)*100)
    return nbCorrectClassification / y_test.size

    
    
###############################
# MLP TensorFlow
###############################



###############################
# Prediction
###############################

def prediction(X,y,T):
    C = np.arange(0.001, 1, 0.01)
    gamma = np.arange(1e-3, 1, 1e-2)

    parameters_svc = {'C':C, 'gamma':gamma}
    
    
    
    
    # Search for best parameters for bagging of SVC
    svc_c = svm.SVC()
    svc_clf = grid_search.GridSearchCV(svc_c, parameters_svc)
    svc_clf.fit(X, y)

    print 'Best params for SVC :'
    print svc_clf.best_params_

    # Crossvalidation score (quite similar to our implementation)
    scores = cross_val_score(svc_clf, X, y, cv=10)
    print 'Score mean : %.5f' % scores.mean() 
   

    y_pred = svc_clf.predict(T)

    return y_pred

###############################
# Feature Transformations
###############################


def remove1Feature(X,y):
    ## Normalization of the data

    for f in range (0,X.shape[1]):
        print 'Removing feature %s' % `f+1`
        X_tild = np.delete(X, f, 1)

        mu_feat = np.mean(X_tild, axis=0)
        sigma_feat = np.std(X_tild, axis=0)

        for i in range (0, X_tild.shape[1]):
            X_tild[:,i] = (X_tild[:,i]-mu_feat[i])/sigma_feat[i]

        CV10(X_tild,y)


################################

def kernel_phog(X, Y):
    # Custom kernel for phog-Histogram features : 
    return X
    
def kernel_1d(X, Y):
    # Custom kernel for 1d features : 
    return X


def main():

    train = True

    # Read training CSV
    X_phog = np.genfromtxt('data/train.csv', delimiter=',', usecols=range(1, 682))
    X_1d = np.genfromtxt('data/train.csv', delimiter=',', usecols=range(682, 697))
    print X_phog.shape
    print X_1d.shape
    X = np.concatenate((X_phog, X_1d), axis=1)
    print X.shape
    
    y = np.genfromtxt('data/train.csv', delimiter=',', usecols=697)

    # # # Read test CSV
    T_ids = np.genfromtxt('data/test_validate.csv', delimiter=',', usecols=0).astype('int')
    T_phog = np.genfromtxt('data/test_validate.csv', delimiter=',', usecols=range(1, 683))
    T_1d = np.genfromtxt('data/test_validate.csv', delimiter=',', usecols=range(683, 697))

    
    T = np.concatenate((T_phog, T_1d), axis=1)


    y_pred = prediction(X, y, T)

   
    #  # # Write test prediction
    # with open('prediction_GBS.csv', 'w') as fp:
    #     a = csv.writer(fp, delimiter=',')
    #     rows = zip(T_ids, y_pred.astype('int'))
    #     a.writerows([['Id', 'Label']])
    #     a.writerows(rows)


if __name__ == "__main__":
    main()